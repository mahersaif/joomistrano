# repository info
set :branch, "menu"

# This may be the same as your `Web` server
role :app, "jodod.webfactional.com"

# directories
set :deploy_to, "/home/jodod/cio_deployment"
# set :public, "#{deploy_to}/public_html"
set :public, "/home/jodod/webapps/cio_staging"
# set :extensions, %w[public template]
